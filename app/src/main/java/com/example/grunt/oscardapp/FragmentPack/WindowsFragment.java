package com.example.grunt.oscardapp.FragmentPack;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.grunt.oscardapp.CaptionedImagesAdapter;
import com.example.grunt.oscardapp.DataWinLinAnd;
import com.example.grunt.oscardapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class WindowsFragment extends Fragment {


    public WindowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        RecyclerView recyclerView=(RecyclerView)inflater.inflate(R.layout.fragment_windows,container,false);

        String[] osName =new String[DataWinLinAnd.windowsArr.length];
        int[] osImages =new int[DataWinLinAnd.windowsArr.length];

        for(int i = 0; i< osName.length; i++){
            osName[i]=DataWinLinAnd.windowsArr[i].getName();
            osImages[i]=DataWinLinAnd.windowsArr[i].getImageResourceId();
        }

        CaptionedImagesAdapter captionedImagesAdapter=new CaptionedImagesAdapter(osName, osImages);
        recyclerView.setAdapter(captionedImagesAdapter);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(gridLayoutManager);



        return recyclerView;
    }

}
