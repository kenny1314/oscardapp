package com.example.grunt.oscardapp;

public class DataWinLinAnd {
    private String name;
    private int imageResourceId;


    public static final DataWinLinAnd[] windowsArr={
            new DataWinLinAnd("Windows XP",R.drawable.xp),
            new DataWinLinAnd("Windows 7",R.drawable.seven),
            new DataWinLinAnd("Windows 10",R.drawable.ten)
    };

    public static final DataWinLinAnd[] LinuxArr={
            new DataWinLinAnd("Ubuntu",R.drawable.ubuntu),
            new DataWinLinAnd("Mate",R.drawable.mate),
            new DataWinLinAnd("Arch",R.drawable.arch)
    };

    public static final DataWinLinAnd[] AndroidArr={
            new DataWinLinAnd("Gingerbread",R.drawable.and23),
            new DataWinLinAnd("Ice Cream Sandwich",R.drawable.and4),
            new DataWinLinAnd("Marshmallow",R.drawable.and6)
    };




    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public DataWinLinAnd(String name, int imageResourceId) {

        this.name = name;
        this.imageResourceId = imageResourceId;
    }
}
