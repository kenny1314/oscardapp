package com.example.grunt.oscardapp.FragmentPack;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.grunt.oscardapp.CaptionedImagesAdapter;
import com.example.grunt.oscardapp.DataWinLinAnd;
import com.example.grunt.oscardapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralFragment extends Fragment {


    static int numberActivity = -1;

    public static void setNumberActivity(int numberActivity) {
        GeneralFragment.numberActivity = numberActivity;
    }

    public GeneralFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_general, container, false);
        String[] osName = null;
        int[] osImages = null;

        switch (GeneralFragment.numberActivity) {
            case 1:
                osName = new String[DataWinLinAnd.windowsArr.length];
                osImages = new int[DataWinLinAnd.windowsArr.length];

                for (int i = 0; i < osName.length; i++) {
                    osName[i] = DataWinLinAnd.windowsArr[i].getName();
                    osImages[i] = DataWinLinAnd.windowsArr[i].getImageResourceId();
                }
                break;
            case 2:
                osName = new String[DataWinLinAnd.LinuxArr.length];
                osImages = new int[DataWinLinAnd.LinuxArr.length];

                for (int i = 0; i < osName.length; i++) {
                    osName[i] = DataWinLinAnd.LinuxArr[i].getName();
                    osImages[i] = DataWinLinAnd.LinuxArr[i].getImageResourceId();
                }
                break;
            case 3:
                osName = new String[DataWinLinAnd.AndroidArr.length];
                osImages = new int[DataWinLinAnd.AndroidArr.length];

                for (int i = 0; i < osName.length; i++) {
                    osName[i] = DataWinLinAnd.AndroidArr[i].getName();
                    osImages[i] = DataWinLinAnd.AndroidArr[i].getImageResourceId();
                }
                break;
        }


        CaptionedImagesAdapter captionedImagesAdapter = new CaptionedImagesAdapter(osName, osImages);
        recyclerView.setAdapter(captionedImagesAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);


        return recyclerView;
    }

}
