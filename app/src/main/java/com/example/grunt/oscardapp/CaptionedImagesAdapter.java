package com.example.grunt.oscardapp;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CaptionedImagesAdapter extends RecyclerView.Adapter<CaptionedImagesAdapter.ViewHolder> {

    private String[] captions;
    private int[] imageIds;

    public CaptionedImagesAdapter(String[] captions, int[] imageIds) {
        this.captions = captions;
        this.imageIds = imageIds;
    }

    @NonNull
    @Override
    public CaptionedImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //макет для представлений
        CardView cardView=(CardView)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_captioned_image,viewGroup,false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull CaptionedImagesAdapter.ViewHolder viewHolder, int i) {

        //заполняем карточку данынми
        CardView cardView=viewHolder.cardView;
        ImageView imageView=(ImageView)cardView.findViewById(R.id.info_image);
        Drawable drawable=ContextCompat.getDrawable(cardView.getContext(),imageIds[i]);
        imageView.setImageDrawable(drawable);

        TextView textView=(TextView)cardView.findViewById(R.id.info_text);
        textView.setText(captions[i]);
    }

    @Override
    public int getItemCount() {
        return captions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        public ViewHolder(CardView itemView) {
            super(itemView);
            cardView=itemView;
        }
    }
}
