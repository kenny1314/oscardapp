package com.example.grunt.oscardapp.ActivityPack;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.support.v7.widget.Toolbar;

import com.example.grunt.oscardapp.FragmentPack.AndroidFragment;
import com.example.grunt.oscardapp.FragmentPack.GeneralFragment;
import com.example.grunt.oscardapp.FragmentPack.LinuxFragment;
import com.example.grunt.oscardapp.FragmentPack.WindowsFragment;
import com.example.grunt.oscardapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter pagerAdapter=new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        TabLayout tabLayout=(TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);


    }

    public void btnClickWin(View view) {
        Intent intent=new Intent(this,WindowsActivity.class);
        startActivity(intent);
    }

    public void btnClickLinux(View view) {
        Intent intent=new Intent(this,LInuxActivity.class);
        startActivity(intent);
    }

    public void btnClickAndroid(View view) {
        Intent intent=new Intent(this,AndroidActivity.class);
        startActivity(intent);
    }

    public void btnClickTest(View view) {
        GeneralFragment.setNumberActivity(3);
        Intent intent=new Intent(this,GeneralActivity.class);
        startActivity(intent);
    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter{

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i){
                case 0:
                    return new WindowsFragment();
                case 1:
                    return new LinuxFragment();
                case 2:
                    return new AndroidFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int position){
            switch (position){
                case 0:
                    return "Windows";
                case 1:
                    return "Linux";
                case 2:
                    return "Android";
            }
            return null;
        }


    }


}
